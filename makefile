CC=mpicc

all: pipefetch nonfetch

pipefetch: pipefetch.c
	$(CC) -o pipefetch -O3 pipefetch.c

nonfetch: nonfetch.c
	$(CC) -o nonfetch -O3 nonfetch.c

clean:
	rm pipefetch nonfetch
