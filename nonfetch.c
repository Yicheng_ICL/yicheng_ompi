#include<stdio.h>
#include<mpi.h>
#include<sys/types.h>
#include<unistd.h>
#include<xmmintrin.h>
#include<string.h>

void print_r(const double *inbuf, const int size, char *name){
	unsigned int i;
	printf("%s\n", name);
	for(i = 0; i < size; i++)
		printf("%.f ", inbuf[i]);
	printf("\n");
}

void cmp(const double *inbuf, const double *obuf, const int size){
        unsigned int i;
        for(i = 0; i < size; i++){
                if(inbuf[i] != obuf[i]){
                        printf("copy not completed\n");
                }
        }
}

#define INPUTSIZE 40960*3
#define stride 64
#define PAGESIZE getpagesize()
#define T2HINT(ptr) _mm_prefetch(ptr, _MM_HINT_T2)
#define T1HINT(ptr) _mm_prefetch(ptr, _MM_HINT_T1)

void do_test(){
	double *inbuf = (double*)malloc(sizeof(double) * INPUTSIZE);
	double *obuf = (double*)malloc(sizeof(double) * INPUTSIZE);

	unsigned int i, z, j;
	for(i = 0; i < INPUTSIZE; i++)
		inbuf[i] = (double)i;

	i = 0;
	j = 0;

	double st, et;
	st = MPI_Wtime();
	memcpy(&obuf[j], &inbuf[i], sizeof(double) * INPUTSIZE);
	et = MPI_Wtime();

	printf("%.7f ms\n", (et - st) * 1000);

	//print_r(inbuf, INPUTSIZE, "inbuf");
	//print_r(obuf, INPUTSIZE, "obuf");
	
	cmp(inbuf, obuf, INPUTSIZE);

	free(inbuf);
	free(obuf);
}

int main(int argc, char **argv){

	do_test();

	return 0;
}
