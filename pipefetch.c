#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<xmmintrin.h>
#include<string.h>
#include<mpi.h>
#include<sys/ipc.h>
#include<sys/shm.h>

void print_r(const double *inbuf, const int size, char *name){
	unsigned int i;
	printf("%s\n", name);
	for(i = 0; i < size; i++)
		printf("%.f ", inbuf[i]);
	printf("\n");
}

void cmp(const double *inbuf, const double *obuf, const int size){
	unsigned int i, unmatch = 0;
	for(i = 0; i < size; i++){
		if(inbuf[i] != obuf[i]){
			unmatch++;
//			printf("%d ", i);
		}
	}

	printf("unmatch %d\n", unmatch);
}

#define INPUTSIZE 40960
#define stride 40960
#define MAX_NUM_FORK 10
#define PAGESIZE getpagesize()
#define T2HINT(ptr) _mm_prefetch(ptr, _MM_HINT_T2)
#define T1HINT(ptr) _mm_prefetch(ptr, _MM_HINT_T1)
void do_test(){
	key_t key = ftok(".", 1);
	key_t okey = ftok(".", 2);

	int status;	
	int shm_inbuf, shm_obuf;
	shm_inbuf = shmget(key, sizeof(double) * INPUTSIZE, IPC_CREAT | 0666);
	shm_obuf = shmget(okey, sizeof(double) * INPUTSIZE, IPC_CREAT | 0666);

	double *inbuf = (double*)shmat(shm_inbuf, NULL, 0);
	double *obuf = (double*)shmat(shm_obuf, NULL, 0);

	int i, j, z;

	for(i = 0; i < INPUTSIZE; i+=1)
		inbuf[i] = (double)i;

	double st, et;
	st = MPI_Wtime();

	j = 0;
	z = 0;
	do{
		//hint to prefetch large size
		T2HINT(&inbuf[j]);
		
		if(z == INPUTSIZE)
			break;
		for(i = 0; i < 10 && z < INPUTSIZE; i++, z+=stride){
			if(fork() == 0){
				//hint to prefetch small size within large size
				T1HINT(&inbuf[z]);
				memcpy(&obuf[z], &inbuf[z], sizeof(double) * stride);
				exit(0);
			}

		}
		j += 10*stride;
	}while(i<INPUTSIZE);
	et = MPI_Wtime();

	printf("%.7fms\n", (et - st) * 1000);

//	print_r(inbuf, INPUTSIZE, "inbuf");
//	print_r(obuf, INPUTSIZE, "obuf");

	cmp(inbuf, obuf, INPUTSIZE);

	shmdt(inbuf);
	shmdt(obuf);
	shmctl(shm_inbuf, IPC_RMID, NULL);
	shmctl(shm_obuf, IPC_RMID, NULL);
}

int main(int argc, char **argv){

	do_test();

	return 0;
}
